//
//  Enemy.h
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Shape.h"

typedef enum {
    MalusTypeBiggerEnemies,
    MalusTypeFastMotion,
    MalusTypeBigger,
    MalusTypeRemoveGoodGuys,
} MalusType;

@interface Malus : Shape

@property (nonatomic) MalusType type;

@end
