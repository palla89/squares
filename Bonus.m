//
//  Enemy.m
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Bonus.h"
#import "Player.h"

@implementation Bonus

-(void)onEnter {
    
    [super onEnter];
}

-(void)bonusInit {
    
    self.spawnSide = arc4random()%4;
    self.shapeType = ShapeTypeBonus;
    self.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"circle.png"];
    self.color = [CCColor cyanColor];
    self.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){self.position, self.contentSize} cornerRadius:0];
    self.position = [GLOBALS spawnNode:self atSpawnSide:self.spawnSide inScene:NO];
    self.physicsBody.collisionGroup = kCollisionGroupBonus;
    self.physicsBody.collisionType = kCollisionTypeBonus;
    self.speed = GLOBALS.currentBonusMalusSpeed;

}

-(id)init {
    
    if ((self = [super init])) {
        
        [self bonusInit];
    }
    return self;
}


@end
