//
//  DiscoverablePic.m
//  Squares
//
//  Created by Alberto Paladino on 08/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "DiscoverablePic.h"
#import "CCNode+Scaling.h"
#import <HexColors.h>
#import "Score.h"

#define kFontName @"Phosphate-Inline"
#define kFontSize 18

@interface DiscoverablePic () <ScorePictureDelegate>
@property (nonatomic,strong) CCEffectPixellate *pixellate;
@property (nonatomic, strong) Score *score;

@end

@implementation DiscoverablePic {
    
    CCLabelTTF *_unlockedPercentualLabel;

}

-(id)init {
    
    if ((self = [super init])) {
        self.score = [Score sharedInstance];
        [self setupImg];
        _unlockedPercentual = 0;
        _unlockedPercentualLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%ld%%",self.unlockedPercentual]
                                                      fontName:kFontName
                                                      fontSize:kFontSize];
        _unlockedPercentualLabel.color =
        [CCColor colorWithUIColor:[UIColor colorWithHexString:@"ECF0F1"]];

        _unlockedPercentualLabel.anchorPoint = ccp(1, 0.5f);
        _unlockedPercentualLabel.position = ccp(self.photoToUnlock.boundingBox.size.width,0);

        [self addChild:_unlockedPercentualLabel];
    }
    
    return self;
}

-(void)setupImg {
    
    self.photoToUnlock = [[CCSprite alloc] initWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"pic.jpg"]];

    [self.photoToUnlock resizeSpriteToWidth:70 andHeight:70];
    
    self.positionType = CCPositionTypeNormalized;
    self.position = ccp(0.075,0.13);
    
    self.pixellate = [CCEffectPixellate effectWithBlockSize:DEFAULT_PIXELLATE_BLOCK];
    self.photoToUnlock.effect = self.pixellate;
    [self addChild:self.photoToUnlock];

}

-(void)setScore:(Score *)score {
    
    _score = score;
    _score.delegatePicture = self;
    
}

-(void)setUnlockedPercentual:(NSInteger)unlockedPercentual {
    
    _unlockedPercentual = unlockedPercentual;
    [_unlockedPercentualLabel setString:[NSString stringWithFormat:@"%ld%%",unlockedPercentual]];
    if (unlockedPercentual % 10 ==0) {
        [self.pixellate setBlockSize:self.pixellate.blockSize-unlockedPercentual/4];
    }

}

-(void)didUpdateUnlockedPhotoPercentual:(NSInteger)percentage {
    
    self.unlockedPercentual = percentage;
    
}

//
//-(void)refresh {
//   
//
//    [self scaleToSize:[CCDirector sharedDirector].viewSize fitType:CCScaleFitAspectFit];
//
//}

@end