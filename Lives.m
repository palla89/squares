//
//  Lives.m
//  shapes
//
//  Created by Alberto Paladino on 25/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Lives.h"

@implementation Lives

-(instancetype)init
{
    if (self = [super init])
    {
    }
    return self;
}

-(void)addLives:(NSInteger)livesNumber {
    GLOBALS.currentLives += livesNumber;
    if (GLOBALS.currentLives>=0)[self.delegate didUpdateLivesLabelWithLives:GLOBALS.currentLives];
    
}

-(void)lifeLost {
    
    [self addLives:-1];

}

-(void)setLives:(NSInteger)newLivesNumber {
    
    GLOBALS.currentLives = newLivesNumber;
    [self.delegate didUpdateLivesLabelWithLives:newLivesNumber];
    
}

@end
