//
//  Score.h
//  shapes
//
//  Created by Alberto Paladino on 23/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@class Score;

@protocol ScoreDelegate <NSObject>
-(void)didUpdateScoreLabelWithScore:(long long)score;
-(void)didUpdateScorePointsLabelWithScorePoints:(long)squaresTaken;
@end

@protocol ScorePictureDelegate <NSObject>
-(void)didUpdateUnlockedPhotoPercentual:(NSInteger)percentage;

@end

@interface Score : CCNode

-(void)startUpdatingScore;
-(void)stopUpdatingScore;
-(void)resetScore;
-(void)addScore:(long long)points;
-(void)setScore:(long long)points;
-(void)setScorePoints:(long)squaresTaken;
-(void)addScorePoints;

+(instancetype)sharedInstance;
@property (nonatomic, weak) id <ScoreDelegate> delegate;
@property (nonatomic, weak) id <ScorePictureDelegate> delegatePicture;

@end