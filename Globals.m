//
//  Globals.m
//  Shapes
//
//  Created by Alberto Paladino on 03/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Globals.h"
#import <GBDeviceInfo.h>

@implementation Globals

+(instancetype)sharedInstance
{
    static Globals *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Globals alloc] init];
    });
    
    return instance;
}

-(CGPoint)spawnNode:(Shape *)node atSpawnSide:(SpawnSide)side inScene:(BOOL)inOrOut {

    CGSize winSize = self.worldRect.size;
    //DLog(@"spawn node globals worldsize: %f %f",winSize.width,winSize.height);
    if (inOrOut) {
        return ccp((arc4random() % (int)(winSize.width - node.boundingBox.size.width)),arc4random() % (int)(winSize.height - node.boundingBox.size.height));
    }
    
    else {
        
        switch (side) {
            case SpawnSideUp:
                return ccp((arc4random() % (int)(node.boundingBox.size.width+winSize.width+fabs(GLOBALS.worldRect.origin.x)))-fabs(GLOBALS.worldRect.origin.x), GLOBALS.worldRect.size.height+node.boundingBox.size.height);
                break;
                
            case SpawnSideDown:
                return ccp((arc4random() % (int)(node.boundingBox.size.width+winSize.width+fabs(GLOBALS.worldRect.origin.x)))-fabs(GLOBALS.worldRect.origin.x), GLOBALS.worldRect.origin.y - node.boundingBox.size.height);
                break;
                
            case SpawnSideLeft:
                return ccp(GLOBALS.worldRect.origin.x-node.boundingBox.size.width,(arc4random() % (int)(node.boundingBox.size.height +fabs(GLOBALS.worldRect.origin.y)+winSize.height))-fabs(GLOBALS.worldRect.origin.y));
                break;
                
            case SpawnSideRight:
                return ccp(winSize.width+node.boundingBox.size.width,(arc4random() % (int)(node.boundingBox.size.height +fabs(GLOBALS.worldRect.origin.y)+winSize.height))-fabs(GLOBALS.worldRect.origin.y));
                break;
                
            default:
                return ccp(winSize.width/2, winSize.height/2);
                break;
        }
        

    }
    
}

- (CCSprite*)blankSpriteWithSize:(CGSize)size
{
    CCSprite *sprite = [CCSprite node];
    GLubyte *buffer = malloc(sizeof(GLubyte)*4);
    for (int i=0;i<4;i++) {buffer[i]=255;}
    CCTexture *tex = [[CCTexture alloc] initWithData:buffer pixelFormat:CCTexturePixelFormat_RGB5A1 pixelsWide:1 pixelsHigh:1 contentSizeInPixels:size contentScale:1.0f];
    [sprite setTexture:tex];
    [sprite setTextureRect:CGRectMake(0, 0, size.width, size.height)];
    free(buffer);
    return sprite;
}


#pragma mark - Utils

-(BOOL)isIphone {
    
    return [GBDeviceInfo deviceInfo].family==GBDeviceFamilyiPhone;
    
}

@end