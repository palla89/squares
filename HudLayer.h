//
//  HudLayer.h
//  shapes
//
//  Created by Alberto Paladino on 22/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "Score.h"
#import "Lives.h"

@interface HudLayer : CCNode <ScoreDelegate,LivesDelegate>

typedef enum {
    ProgressBarPositionUpper,
    ProgressBarPositionLower,
} ProgressBarPosition;

@property (nonatomic, strong) Lives *lives;

-(void)showProgressBarOfType:(ProgressBarPosition)position Color:(CCColor*)color ticking:(BOOL)ticking timerToEnd:(CCTime)time;
-(void)stopAndRemoveProgressBar:(ProgressBarPosition)position;

@end
