//
//  CCActionFollow+Smoother.m
//  shapes
//
//  Created by Alberto Paladino on 21/11/15.
//  Copyright © 2015 Apportable. All rights reserved.
//

#import "CCActionFollow+Smoother.h"
#import <CCAction.h>

@implementation CCActionFollow_Smoother


#define CLAMP(x,y,z) MIN(MAX(x,y),z)
-(void) step:(CCTime) dt
{

    CGPoint pos;
    if(_boundarySet)
    {
        // whole map fits inside a single screen, no need to modify the position - unless map boundaries are increased
        
        if(_boundaryFullyCovered)
            return;
        
        CGPoint tempPos = ccpSub( _halfScreenSize, _followedNode.position);
        pos = ccp(CLAMP(tempPos.x, _leftBoundary, _rightBoundary), CLAMP(tempPos.y, _bottomBoundary, _topBoundary));
        
    }
    else {

        CCNode *n = (CCNode*)_target;
        
        float s = n.scale;
        
        pos = ccpSub( _halfScreenSize, _followedNode.position );
        
        pos.x *= s;
        
        pos.y *= s;
        
    }
    
    CGPoint moveVect;
    
    
    
    CGPoint oldPos = [(CCNode*)_target position];
    
    double dist = ccpDistance(pos, oldPos);
    
    if (dist > 1){
        
        moveVect = ccpMult(ccpSub(pos,oldPos),0.02); //0.05 is the smooth constant.
        
        oldPos = ccpAdd(oldPos, moveVect);
        
        [(CCNode*)_target setPosition:oldPos];
        
    }
    
#undef CLAMP
}


@end
