#import "MainScene.h"
#import <CCPhysics+ObjectiveChipmunk.h>
#import "Player.h"
#import "Enemy.h"
#import "ScorePoint.h"
#import "Bonus.h"
#import "Malus.h"
#import "DiscoverablePic.h"
#import <GBDeviceInfo.h>
#import "HudLayer.h"
#import "Score.h"
#import "Lives.h"
#import "Countdown.h"
#import "WinLosePauseDialog.h"
#import <HexColors.h>
#import "CCActionFollow+Smoother.h"
#import "CCTexture_Private.h"

@interface MainScene () <CCPhysicsCollisionDelegate,ShapeDelegate,CountdownDelegate>

@property (nonatomic, strong) Player *player;
@property (nonatomic, strong) Enemy *enemy;
@property (nonatomic, strong) ScorePoint *scorePoint;
@property (nonatomic, strong) Score *currentScore;
@property (nonatomic, strong) NSMutableSet *enemies;
@property (nonatomic, strong) NSMutableSet *scorePoints;
@property (nonatomic) NSInteger currentMinEnemiesNumber;
@property (nonatomic) NSInteger currentMinScorePointsNumber;
@property (nonatomic, strong) DiscoverablePic *discoverablePic;
@property (nonatomic, strong) CCScene *gameLevel;

@property (nonatomic, readwrite) GameState gameState;

//particles
@property (nonatomic, strong) CCParticleSystem *backgroundEffect;
@property (nonatomic, strong) CCParticleSystem *backgroundEffectYellow;

@end

@implementation MainScene

CCPhysicsNode *_physicsWorld;
HudLayer *_hud;
Score *_score;
Lives *_lives;
DiscoverablePic *_discoverablePic;
CCSprite *_bg;

-(instancetype)init
{
    if((self = [super init])){
        
        
        self.userInteractionEnabled = YES;
        self.gameState = GameStateUninitialized;
        self.gameLevel = [[CCScene alloc]init];
        CGSize gameLevelSize = GAMEFIELD_SIZE;
        self.gameLevel.contentSize = gameLevelSize;
        [self.gameLevel setPosition:ccp((WINSIZE.width-gameLevelSize.width)/2.0f, (WINSIZE.height-gameLevelSize.height)/2.0f)];
        
        
        
        [self addChild:self.gameLevel];
        GLOBALS.worldRect = [self.gameLevel boundingBox];
        _discoverablePic = [[DiscoverablePic alloc]init];
        [self addChild:_discoverablePic];
        
        [self initializeHUD];
        
        Countdown *countdown = [[Countdown alloc]initWithTime:3 andMessage:@"yo"];
        countdown.delegate = self;
        [self addChild:countdown z:2000];
        
        self.backgroundEffect =  GLOBALS.isIphone ? [CCParticleSystem particleWithFile:@"galaxy_iphone.plist"] : [CCParticleSystem particleWithFile:@"galaxy_ipad.plist"];
        self.backgroundEffect.particlePositionType = CCParticleSystemPositionTypeRelative;
        self.backgroundEffect.positionType = CCPositionTypeNormalized;
        self.backgroundEffect.position = ccp(0.5, 0.5);
        [self addChild:self.backgroundEffect];
        
        self.backgroundEffectYellow =  GLOBALS.isIphone ? [CCParticleSystem particleWithFile:@"galaxy_iphone_yellow.plist"] : [CCParticleSystem particleWithFile:@"galaxy_ipad_yellow.plist"];
        self.backgroundEffectYellow.particlePositionType = CCParticleSystemPositionTypeRelative;
        self.backgroundEffectYellow.positionType = CCPositionTypeNormalized;
        self.backgroundEffectYellow.position = ccp(0.5, 0.5);
        [self.backgroundEffectYellow stopSystem];
        [self addChild:self.backgroundEffectYellow];
        
        _physicsWorld = [CCPhysicsNode node];
        _physicsWorld.gravity = ccp(0,0);
        _physicsWorld.debugDraw = NO;
        _physicsWorld.collisionDelegate = self;
        [self addChild:_physicsWorld];

        
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"elements.plist"];

        CCParallaxNode *parallaxNode = [[CCParallaxNode alloc]init];
        parallaxNode.position = ccp(WINSIZE.width/2,WINSIZE.height/2);

        //luna in alto a sx
        CCSprite *moon = [CCSprite spriteWithImageNamed:@"moon.png"];
        moon.texture.antialiased = NO;
        moon.scale = 3;
        float moonXOffset = (gameLevelSize.width/GAMEFIELD_SCALE_COEFFICIENT) * 0.05;
        float moonYOffset = (gameLevelSize.height/GAMEFIELD_SCALE_COEFFICIENT) * 0.95;
        [parallaxNode addChild:moon z:2 parallaxRatio:ccp(0.1f,0.1f) positionOffset:ccp(moonXOffset,moonYOffset)];
        
        //terra in basso al centro
        CCSprite *earth = [CCSprite spriteWithImageNamed:@"earth.png"];
        earth.texture.antialiased = NO;
        earth.scale = 4;
        float earthXOffset = (gameLevelSize.width/GAMEFIELD_SCALE_COEFFICIENT) * 0.5;
        float earthYOffset = (gameLevelSize.height/GAMEFIELD_SCALE_COEFFICIENT) * -0.6;
        
        [parallaxNode addChild:earth z:2 parallaxRatio:ccp(0.2f,0.2f) positionOffset:ccp(earthXOffset,earthYOffset)];

        CCSprite *star = [CCSprite spriteWithImageNamed:@"star.png"];
        star.texture.antialiased = NO;
        star.scale = 1;
        float starXOffset = (gameLevelSize.width/GAMEFIELD_SCALE_COEFFICIENT) * 0.85;
        float starYOffset = (gameLevelSize.height/GAMEFIELD_SCALE_COEFFICIENT) * 0.85;
        
        [parallaxNode addChild:star z:2 parallaxRatio:ccp(0.05f,0.05f) positionOffset:ccp(starXOffset,starYOffset)];
        
        //sfondo nuvoloso ovunque
        _bg = [CCSprite spriteWithImageNamed:@"bg.png"];
        _bg.opacity = 0.1;
        _bg.textureRect = CGRectMake(-4096, 4096, 8192, 8192);
        ccTexParams params = (ccTexParams){GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT};
        [_bg.texture setTexParameters:&params];
        
        [parallaxNode addChild:_bg z:3 parallaxRatio:ccp(2,2) positionOffset:ccp(0,0)];
        
        
        
        [_physicsWorld addChild:parallaxNode];
        
        
        DLog(@"where is the moon? %f %f",moon.position.x,moon.position.y);
        
        
        
    }
    
    return self;
}

-(void)startGame {
    
    if (self.gameState == GameStateUninitialized) {
        self.gameState = GameStatePlaying;
        self.player = [[Player alloc] init];
        [_physicsWorld addChild:self.player];
        
        self.enemies = [[NSMutableSet alloc] init];
        self.scorePoints = [[NSMutableSet alloc] init];
        
        [self schedule:@selector(spawnBonus:) interval:BONUS_MALUS_SPAWN_DELAY];
        [self schedule:@selector(spawnMalus:) interval:BONUS_MALUS_SPAWN_DELAY];
        [_score startUpdatingScore];

        CGRect bounds = [self.gameLevel boundingBox];
        // may need to do this:
        // bounds.origin = CGPointZero;
        NSLog(@"Bounds: %@", NSStringFromCGRect(bounds));
        
        CCActionFollow_Smoother *follow = [CCActionFollow_Smoother actionWithTarget:self.player worldBoundary:bounds];
        
        [_physicsWorld runAction:follow];
        
    }
    
    


}

-(void)resetScene {
    [_score stopUpdatingScore];
    
    CCScene *mainScene = [[MainScene alloc]init];

    [[CCDirector sharedDirector] replaceScene:mainScene];
    
}

-(void)resetGame {
    
    [_score resetScore];
    [_lives setLives:LIVES_NUMBER];
    
    
}


-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    
    CGPoint touchLocation = [touch locationInNode:self];
    DLog(@"MainSceneLoc : %f %f",touchLocation.x,touchLocation.y);
    CGPoint touchLocationz = [touch locationInNode:self.gameLevel];
    DLog(@"GameSceneLoc : %f %f",touchLocationz.x,touchLocationz.y);
    
    
}

-(void)setGameState:(GameState)gameState {
    
    _gameState = gameState;
    
    switch (gameState) {
        case GameStateLost:
            [self.player removeFromParent];
            [_lives lifeLost];
            if (GLOBALS.currentLives >= 0) {
               
                [self resetScene];
                
            } else {
                //game over
                [_score stopUpdatingScore];
                GameStats *currentStats = [[GameStats alloc]init];
                
                WinLosePauseDialog *wlDialog =
                [[WinLosePauseDialog alloc]
                 initDialogWithGameState:gameState andStats:currentStats];
                [self addChild:wlDialog];
                
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - Countdown delegate

-(void)countdownDidEnd {
    
    [self startGame];
    
}

#pragma mark - HUD

-(void)initializeHUD
{
    _hud = [[HudLayer alloc] init];
    _lives = [[Lives alloc] init];
    _score = [Score sharedInstance];
    _hud.lives = _lives;
    
    [self addChild:_hud z:1000];
}

#pragma mark - Spawns

-(void)spawnBonus:(CCTime)delta {
    
    int bonusType = arc4random()%BONUS_NUMBER;
    Bonus *bonus = [[Bonus alloc]init];
    bonus.type = bonusType;
    bonus.delegate = self;
    [_physicsWorld addChild:bonus];
    
}

-(void)spawnMalus:(CCTime)delta {
    
    int malusType = arc4random()%MALUS_NUMBER;
    Malus *malus = [[Malus alloc]init];
    malus.type = malusType;
    malus.delegate = self;
    [_physicsWorld addChild:malus];
    
}

#pragma mark - Update method

CGPoint newBgPos;
-(void)update:(CCTime)delta {
    
    if (self.gameState == GameStatePlaying) {
        if (self.enemies.count < GLOBALS.minEnemies) {
            
            Enemy *enemy = [[Enemy alloc]init];
            enemy.delegate = self;
            enemy.scale = GLOBALS.enemyScale;
            [self.enemies addObject:enemy];
            [_physicsWorld addChild:enemy];
            
        }
        if (self.scorePoints.count < GLOBALS.minScorePoints) {
            
            ScorePoint *scorePoint = [[ScorePoint alloc] init];
            scorePoint.delegate = self;
            scorePoint.scale = GLOBALS.scorePointScale;
            [self.scorePoints addObject:scorePoint];
            [_physicsWorld addChild:scorePoint];
        }
        //DLog(@"PG POS: %f %f",self.player.position.x,self.player.position.y);
       // self.player.position = ccp(self.player.position.x - 80 * (CGFloat)delta, self.player.position.y);
        //_physicsWorld.position = ccp(gamePhysicsNode.position.x - scrollSpeed * CGFloat(delta), gamePhysicsNode.position.y)
    }
    
}

#pragma mark - Shape delegate methods

-(void)didRemoveShapeFromParent:(Shape *)shape ofType:(ShapeType)shapeType {
    
    switch (shapeType) {
        case ShapeTypeEnemy:
            [self.enemies removeObject:shape];
            break;
        case ShapeTypeScorePoint:
            [self.scorePoints removeObject:shape];
            break;
        default:
            break;
    }
    
}

#pragma mark - Collisions

//collisione tra player e scorepoints
-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)player scorePointCollision:(CCNode *)scorePoint {
    
    if (((Shape*)scorePoint).firstCollided) return NO;
    ((Shape*)scorePoint).firstCollided = YES;
    CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:ZOOM_SCALE_FACTOR];
    CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
        CCPhysicsShape* shape = self.player.physicsBody.shapeList;
        [shape rescaleShape:[self.player nonRigidTransform]];
    }];
    CCActionSequence *scalePlayer = [CCActionSequence actions:scaleBy,scaleBody, nil];
    CCActionScaleTo * absorbEnemy = [CCActionScaleTo actionWithDuration:.3 scale:0];
    CCActionCallBlock *removeEnemy = [CCActionCallBlock actionWithBlock:^{
        [scorePoint removeFromParent];
        [self.scorePoints removeObject:scorePoint];
        if (self.player.scale < SCALE_LIMIT_UPPER) {
            [self.player runAction:scalePlayer];
        }
        
    }];
    CCActionSequence *seq = [CCActionSequence actionOne:absorbEnemy two:removeEnemy];
    [scorePoint runAction:seq];
    
    [_score addScorePoints]; //GLOBALS.scorePointsTaken++;
    [_score addScore:SCOREPOINT_POINTS];
    
    if ((GLOBALS.scorePointsTaken % LEVEL_UP_TARGET) == 0) {
        GLOBALS.minEnemies+=ENEMIES_INCREMENT;
        GLOBALS.currentBonusMalusSpeed += SHAPE_SPEED_INCREMENT;
        GLOBALS.currentSpeed += SHAPE_SPEED_INCREMENT;
        GLOBALS.currentLevel++;
        DLog(@"aumento difficoltà: livello %ld!", (long)GLOBALS.currentLevel);
    }
    
    return YES;
}

//collisione tra player e nemici
-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)player enemyCollision:(CCNode *)enemy {
    
    CCParticleSystem *deathEffect =  [CCParticleSystem particleWithFile:@"Death.plist"];
    deathEffect.position = self.player.position;
    [_physicsWorld addChild:deathEffect];
    [self.player removeFromParent];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.gameState = GameStateLost;
    });
    //self.gameState = GameStateLost;
    
    
    return YES;
}

#pragma mark - Bonus

//collisione tra player e bonus
-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)player bonusCollision:(CCNode *)bonus {
    
    Bonus *bbonus = (Bonus*)bonus;
    if (bbonus.firstCollided) return NO;
    bbonus.firstCollided = YES;
    NSNumber *type = @(bbonus.type);
    [_score addScore:BONUS_POINTS];

    [_hud stopAndRemoveProgressBar:ProgressBarPositionUpper];
    [_hud showProgressBarOfType:ProgressBarPositionUpper Color:[CCColor colorWithUIColor:[UIColor colorWithHexString:@"00B16A"]] ticking:YES timerToEnd:DEFAULT_BONUS_DURATION];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(cancelBonus:) object:type];
    switch (bbonus.type) {
        case BonusTypeSmaller:{
            if ((self.player.scale * BONUS_RESCALE) > SCALE_LIMIT_LOWER) {
                CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:BONUS_RESCALE];
                CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
                    CCPhysicsShape* shape = self.player.physicsBody.shapeList;
                    [shape rescaleShape:[self.player nonRigidTransform]];
                }];
                CCActionSequence *scalePlayer = [CCActionSequence actions:scaleBy,scaleBody, nil];
                [self.player runAction:scalePlayer];
            }
        }
            break;
        case BonusTypeSlowMotion:{
            
            GLOBALS.currentAcceleration = SLOWMOTION_ACCELERATION;
            [self resetBackgroundEffect];
            self.backgroundEffect.radialAccel = 3.0;
            for (Enemy *enemy in self.enemies) {
                enemy.slowMotionMode = YES;
                enemy.fastMotionMode = NO;
            }
            for (ScorePoint *scorePoint in self.scorePoints) {
                scorePoint.slowMotionMode = YES;
                scorePoint.fastMotionMode = NO;
            }
        }
            break;
        case BonusTypeInvulnerability: {
            self.player.invulnerable = YES;
            [self.backgroundEffect stopSystem];
            [self.backgroundEffectYellow resetSystem];
            
        }
            break;
            
        case BonusTypeRemoveBadGuys:
            if (GLOBALS.minEnemies >0) self.currentMinEnemiesNumber = GLOBALS.minEnemies;
            GLOBALS.minEnemies = 0;
            for (Enemy *enemy in self.enemies) {
                [enemy removeFromParent];
            }
            [self.enemies removeAllObjects];
            
            break;
        default:
            break;
    }
    [self performSelector:@selector(cancelBonus:) withObject:type afterDelay:DEFAULT_BONUS_DURATION];
    [bonus removeFromParent];
    
    return YES;
}

-(void)cancelBonus:(NSNumber*)bonus {
    
    BonusType bonusType = [bonus intValue];
    //DLog(@"end bonus %d",bonusType);

    switch (bonusType) {
            
        case BonusTypeSmaller: {
            
            CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:MALUS_RESCALE];
            CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
                CCPhysicsShape* shape = self.player.physicsBody.shapeList;
                [shape rescaleShape:[self.player nonRigidTransform]];
            }];
            CCActionSequence *scalePlayer = [CCActionSequence actions:scaleBy,scaleBody, nil];
            [self.player runAction:scalePlayer];
            
        }
            
        case BonusTypeSlowMotion: {
            
            GLOBALS.currentAcceleration = DEFAULT_ACCELERATION;
            [self resetBackgroundEffect];
            for (Enemy *enemy in self.enemies) {
                enemy.slowMotionMode = NO;
                enemy.fastMotionMode = NO;
            }
            for (ScorePoint *scorePoint in self.scorePoints) {
                scorePoint.slowMotionMode = NO;
                scorePoint.fastMotionMode = NO;
            }
        }
            break;
            
        case BonusTypeInvulnerability:
            self.player.invulnerable = NO;
            [self.backgroundEffectYellow stopSystem];
            [self.backgroundEffect resetSystem];
            break;
            
        case BonusTypeRemoveBadGuys:
            GLOBALS.minEnemies = self.currentMinEnemiesNumber;
            break;
            
        default:
            break;
    }
    
}

#pragma mark - Malus

//collisione tra player e malus
-(BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair playerCollision:(CCNode *)player malusCollision:(CCNode *)malus {
    
    Malus *mmalus = (Malus*)malus;
    if (mmalus.firstCollided) return NO;
    mmalus.firstCollided = YES;
    
    NSNumber *type = @(mmalus.type);
    [_score addScore:MALUS_POINTS];
    
    [_hud stopAndRemoveProgressBar:ProgressBarPositionLower];
    [_hud showProgressBarOfType:ProgressBarPositionLower Color:[CCColor colorWithUIColor:[UIColor colorWithHexString:@"EF4836"]] ticking:YES timerToEnd:DEFAULT_MALUS_DURATION];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(cancelMalus:) object:type];
    switch (mmalus.type) {
        case MalusTypeBigger:{
            if ((self.player.scale * MALUS_RESCALE) < SCALE_LIMIT_UPPER) {
                CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:MALUS_RESCALE];
                CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
                    CCPhysicsShape* shape = self.player.physicsBody.shapeList;
                    [shape rescaleShape:[self.player nonRigidTransform]];
                }];
                CCActionSequence *scalePlayer = [CCActionSequence actions:scaleBy,scaleBody, nil];
                [self.player runAction:scalePlayer];
            }
        }
            break;
        case MalusTypeFastMotion:{
            
            GLOBALS.currentAcceleration = FASTMOTION_ACCELERATION;
            [self resetBackgroundEffect];
            self.backgroundEffect.speed = 300;
            for (Enemy *enemy in self.enemies) {
                enemy.slowMotionMode = NO;
                enemy.fastMotionMode = YES;
            }
            for (ScorePoint *scorePoint in self.scorePoints) {
                scorePoint.slowMotionMode = NO;
                scorePoint.fastMotionMode = YES;
            }
        }
            break;
        case MalusTypeBiggerEnemies: {
            for (Enemy *enemy in self.enemies) {
                CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:MALUS_RESCALE];
                CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
                    CCPhysicsShape* shape = enemy.physicsBody.shapeList;
                    [shape rescaleShape:[enemy nonRigidTransform]];
                    
                }];
                CCActionSequence *scaleEnemy = [CCActionSequence actions:scaleBy,scaleBody, nil];
                [enemy runAction:scaleEnemy];
                GLOBALS.enemyScale = MALUS_RESCALE;
            }
            
        }
            break;
            
        case MalusTypeRemoveGoodGuys:
            if (GLOBALS.minScorePoints >0) self.currentMinScorePointsNumber = GLOBALS.minScorePoints;
            GLOBALS.minScorePoints = 0;
            for (ScorePoint *scorePoint in self.scorePoints) {
                [scorePoint removeFromParent];
            }
            [self.scorePoints removeAllObjects];
            
            break;
        default:
            break;
    }
    [self performSelector:@selector(cancelMalus:) withObject:type afterDelay:DEFAULT_MALUS_DURATION];
    [malus removeFromParent];
    
    return YES;
}

-(void)cancelMalus:(NSNumber*)malus {
    
    MalusType malusType = [malus intValue];
    //DLog(@"stopped malus %d",malusType);
    switch (malusType) {
            
        case MalusTypeBigger: {
            
            CCActionScaleBy *scaleBy = [CCActionScaleBy actionWithDuration:0.3 scale:BONUS_RESCALE];
            CCActionCallBlock *scaleBody = [CCActionCallBlock actionWithBlock:^{
                CCPhysicsShape* shape = self.player.physicsBody.shapeList;
                [shape rescaleShape:[self.player nonRigidTransform]];
            }];
            CCActionSequence *scalePlayer = [CCActionSequence actions:scaleBy,scaleBody, nil];
            [self.player runAction:scalePlayer];
            
        }
            
        case MalusTypeFastMotion: {
            
            GLOBALS.currentAcceleration = DEFAULT_ACCELERATION;
            [self resetBackgroundEffect];

            for (Enemy *enemy in self.enemies) {
                enemy.slowMotionMode = NO;
                enemy.fastMotionMode = NO;
            }
            for (ScorePoint *scorePoint in self.scorePoints) {
                scorePoint.slowMotionMode = NO;
                scorePoint.fastMotionMode = NO;
            }
        }
            break;
        
        case MalusTypeBiggerEnemies:
            GLOBALS.enemyScale = DEFAULT_SCALE_VALUE;
            break;
            
        case MalusTypeRemoveGoodGuys:
            GLOBALS.minScorePoints = self.currentMinScorePointsNumber;
            break;
            
        default:
            break;
    }
}

-(void)resetBackgroundEffect {
    
    self.backgroundEffect.speed = PARTICLE_BACKGROUND_DEFAULT_SPEED;
    self.backgroundEffect.radialAccel = PARTICLE_BACKGROUND_DEFAULT_RADIALACC;

}

@end