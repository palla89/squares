/*
 * SpriteBuilder: http://www.spritebuilder.org
 *
 * Copyright (c) 2012 Zynga Inc.
 * Copyright (c) 2013 Apportable Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#import "cocos2d.h"

#import "AppDelegate.h"
#import "MainScene.h"
#import "MenuScene.h"

@implementation AppController

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupCocos2dWithOptions:@{
			CCSetupTabletScale2X: @(YES),
            CCSetupScreenOrientation: CCScreenOrientationLandscape,
            //CCSetupFixedUpdateInterval: @(1.0f/120.0f)
		}];
    
    if (GLOBALS.isIphone) DLog(@"iPhone detected");
    
    GLOBALS.globalShapeSpeed = DEFAULT_SHAPE_SPEED;
    GLOBALS.minEnemies = DEFAULT_ENEMIES_NUMBER;
    GLOBALS.minScorePoints = DEFAULT_SCOREPOINTS_NUMBER;
    GLOBALS.currentSpeed = DEFAULT_SHAPE_SPEED;
    GLOBALS.currentBonusMalusSpeed = DEFAULT_BONUS_MALUS_SPEED;
    GLOBALS.enemyScale = DEFAULT_SCALE_VALUE;
    GLOBALS.scorePointScale = DEFAULT_SCALE_VALUE;
    GLOBALS.currentAcceleration = 1;
    GLOBALS.scorePointsTaken = 0;
    GLOBALS.currentLevel = 1;
    GLOBALS.currentScoreIncrement = DEFAULT_SCORE_INCREMENT;
    GLOBALS.currentLives = LIVES_NUMBER;
    GLOBALS.currentPercentage = 0;


    
    return YES;
}


- (CCScene*) startScene
{
    return [MenuScene node];
    //return [MainScene node];
}


@end
