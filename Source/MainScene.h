typedef enum GameState
{
    GameStateUninitialized,
    GameStatePlaying,
    GameStatePaused,
    GameStateWon,
    GameStateLost
    
} GameState;

@interface MainScene : CCScene

-(void)resetGame;

@end
