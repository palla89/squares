//
//  Enemy.h
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Shape.h"

typedef enum {
    BonusTypeInvulnerability,
    BonusTypeSlowMotion,
    BonusTypeSmaller,
    BonusTypeRemoveBadGuys,
} BonusType;

@interface Bonus : Shape

@property (nonatomic) BonusType type;

@end
