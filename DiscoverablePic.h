//
//  DiscoverablePic.h
//  Squares
//
//  Created by Alberto Paladino on 08/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCSprite.h"

@interface DiscoverablePic : CCNode

@property (nonatomic,strong) CCSprite *photoToUnlock;
@property (nonatomic,assign) NSInteger unlockedPercentual;

@end
