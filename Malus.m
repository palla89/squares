//
//  Enemy.m
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Malus.h"
#import "Player.h"

@implementation Malus

-(void)onEnter {
    
    [super onEnter];
}

-(void)malusInit {
    
    self.spawnSide = arc4random()%4;
    self.shapeType = ShapeTypeMalus;
    self.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"circle.png"];
    self.color = [CCColor purpleColor];
    self.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){self.position, self.contentSize} cornerRadius:0];
    self.position = [GLOBALS spawnNode:self atSpawnSide:self.spawnSide inScene:NO];
    self.physicsBody.collisionGroup = kCollisionGroupBonus;
    self.physicsBody.collisionType = kCollisionTypeMalus;
    self.speed = GLOBALS.currentBonusMalusSpeed;

}

-(id)init {
    
    if ((self = [super init])) {
        
        [self malusInit];
        
    }
    return self;
}


@end
