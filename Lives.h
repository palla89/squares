//
//  Lives.h
//  shapes
//
//  Created by Alberto Paladino on 25/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@class Lives;

@protocol LivesDelegate <NSObject>
-(void)didUpdateLivesLabelWithLives:(NSInteger)livesNumber;
@end

@interface Lives : CCNode

-(void)setLives:(NSInteger)newLivesNumber;
-(void)addLives:(NSInteger)livesNumber;
-(void)lifeLost;

@property (nonatomic, weak) id <LivesDelegate> delegate;
@end
