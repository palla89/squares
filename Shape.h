//
//  Shape.h
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCSprite.h"

typedef enum {
    SpawnSideUp,
    SpawnSideDown,
    SpawnSideLeft,
    SpawnSideRight
} SpawnSide;

typedef enum {
    ShapeTypeGeneric,
    ShapeTypePlayer,
    ShapeTypeEnemy,
    ShapeTypeScorePoint,
    ShapeTypeBonus,
    ShapeTypeMalus
} ShapeType;

@class Shape;

@protocol ShapeDelegate <NSObject>
-(void)didRemoveShapeFromParent:(Shape*)shape ofType:(ShapeType)shapeType;

@end

@interface Shape : CCSprite

-(CGAffineTransform)nonRigidTransform; // private in CCNode

@property (nonatomic) SpawnSide spawnSide;
@property (nonatomic) ShapeType shapeType;

//behavior
@property (nonatomic) CGFloat rotationSpeed;
@property (nonatomic) CGFloat speed;
@property (nonatomic) BOOL slowMotionMode;
@property (nonatomic) BOOL fastMotionMode;
@property (nonatomic) BOOL firstCollided;

@property (nonatomic, weak) id <ShapeDelegate> delegate;

@end
