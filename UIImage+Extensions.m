//
//  UIImage+Extensions.m
//
//  Created by Hardy Macia on 7/1/09.
//  Copyright 2009 Catamount Software. All rights reserved.
//

@import UIKit;

#import "UIImage+Extensions.h"
#import <objc/runtime.h>


@implementation UIImage (CS_Extensions)

-(CGFloat) DegreesToRadians:(CGFloat)degrees {return degrees * M_PI / 180;};
-(CGFloat) RadiansToDegrees:(CGFloat)radians {return radians * 180/M_PI;};

-(UIImage *)imageAtRect:(CGRect)rect
{
   
   CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
   UIImage* subImage = [UIImage imageWithCGImage: imageRef];
   CGImageRelease(imageRef);
   
   return subImage;
   
}

- (UIImage *)imageByScalingProportionallyToMinimumSize:(CGSize)targetSize {
   
   UIImage *sourceImage = self;
   UIImage *newImage = nil;
   
   CGSize imageSize = sourceImage.size;
   CGFloat width = imageSize.width;
   CGFloat height = imageSize.height;
   
   CGFloat targetWidth = targetSize.width;
   CGFloat targetHeight = targetSize.height;
   
   CGFloat scaleFactor = 0.0;
   CGFloat scaledWidth = targetWidth;
   CGFloat scaledHeight = targetHeight;
   
   CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
   
   if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
      
      CGFloat widthFactor = targetWidth / width;
      CGFloat heightFactor = targetHeight / height;
      
      if (widthFactor > heightFactor) 
         scaleFactor = widthFactor;
      else
         scaleFactor = heightFactor;
      
      scaledWidth  = width * scaleFactor;
      scaledHeight = height * scaleFactor;
      
      // center the image
      
      if (widthFactor > heightFactor) {
         thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
      } else if (widthFactor < heightFactor) {
         thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
      }
   }
   
   
   // this is actually the interesting part:
   
   UIGraphicsBeginImageContext(targetSize);
   
   CGRect thumbnailRect = CGRectZero;
   thumbnailRect.origin = thumbnailPoint;
   thumbnailRect.size.width  = scaledWidth;
   thumbnailRect.size.height = scaledHeight;
   
   [sourceImage drawInRect:thumbnailRect];
   
   newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   
   if(newImage == nil) NSLog(@"could not scale image");
   
   
   return newImage ;
}


- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize {
   
   UIImage *sourceImage = self;
   UIImage *newImage = nil;
   
   CGSize imageSize = sourceImage.size;
   CGFloat width = imageSize.width;
   CGFloat height = imageSize.height;
   
   CGFloat targetWidth = targetSize.width;
   CGFloat targetHeight = targetSize.height;
   
   CGFloat scaleFactor = 0.0;
   CGFloat scaledWidth = targetWidth;
   CGFloat scaledHeight = targetHeight;
   
   CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
   
   if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
      
      CGFloat widthFactor = targetWidth / width;
      CGFloat heightFactor = targetHeight / height;
      
      if (widthFactor < heightFactor) 
         scaleFactor = widthFactor;
      else
         scaleFactor = heightFactor;
      
      scaledWidth  = width * scaleFactor;
      scaledHeight = height * scaleFactor;
      
      // center the image
      
      if (widthFactor < heightFactor) {
         thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
      } else if (widthFactor > heightFactor) {
         thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
      }
   }
   
   
   // this is actually the interesting part:
   
   UIGraphicsBeginImageContext(targetSize);
   
   CGRect thumbnailRect = CGRectZero;
   thumbnailRect.origin = thumbnailPoint;
   thumbnailRect.size.width  = scaledWidth;
   thumbnailRect.size.height = scaledHeight;
   
   [sourceImage drawInRect:thumbnailRect];
   
   newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   
   if(newImage == nil) NSLog(@"could not scale image");
   
   
   return newImage ;
}


- (UIImage *)imageByScalingToSize:(CGSize)targetSize {
   
   UIImage *sourceImage = self;
   UIImage *newImage = nil;
   
   //   CGSize imageSize = sourceImage.size;
   //   CGFloat width = imageSize.width;
   //   CGFloat height = imageSize.height;
   
   CGFloat targetWidth = targetSize.width;
   CGFloat targetHeight = targetSize.height;
   
   //   CGFloat scaleFactor = 0.0;
   CGFloat scaledWidth = targetWidth;
   CGFloat scaledHeight = targetHeight;
   
   CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
   
   // this is actually the interesting part:
   
   UIGraphicsBeginImageContext(targetSize);
   
   CGRect thumbnailRect = CGRectZero;
   thumbnailRect.origin = thumbnailPoint;
   thumbnailRect.size.width  = scaledWidth;
   thumbnailRect.size.height = scaledHeight;
   
   [sourceImage drawInRect:thumbnailRect];
   
   newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   
   if(newImage == nil) NSLog(@"could not scale image");
   
   
   return newImage ;
}


- (UIImage *)imageRotatedByRadians:(CGFloat)radians
{
    return [self imageRotatedByDegrees:[self RadiansToDegrees:radians ]];
}

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees 
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation([self DegreesToRadians:degrees]);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, [self DegreesToRadians:degrees]);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
   
}

-(UIImage*)imageOverlayWithColor:(UIColor*)col
{
    
    __weak UIImage *this = self;
    UIGraphicsBeginImageContextWithOptions(this.size, NO, [UIScreen mainScreen].scale);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, 0, this.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    CGFloat r,g,b,a;
    [col getRed:&r green:&g blue:&b alpha:&a];
    CGContextSetRGBFillColor(ctx, r, g, b, a);
    CGContextFillRect(ctx, CGRectMake(0, 0, this.size.width, this.size.height));            
    CGContextSetBlendMode(ctx, kCGBlendModeOverlay);
    CGContextDrawImage(ctx, CGRectMake(0, 0, this.size.width, this.size.height), this.CGImage);
    UIImage *imgOut = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return imgOut;
}

-(NSInteger)byteSize
{
    return CGImageGetBytesPerRow(self.CGImage)*CGImageGetHeight(self.CGImage)+class_getInstanceSize([UIImage class]);
}


@end;