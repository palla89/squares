//
//  GameStats.h
//  shapes
//
//  Created by Alberto Paladino on 06/09/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameStats : NSObject

@property (nonatomic, assign) NSInteger squaresTaken;
@property (nonatomic, assign) NSInteger percentUnlocked;

@end
