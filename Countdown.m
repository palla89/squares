//
//  Countdown.m
//  shapes
//
//  Created by Alberto Paladino on 26/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Countdown.h"

@interface Countdown ()

@property (nonatomic, strong) CCLabelBMFont *countdownLabel;
@property (nonatomic, assign) NSInteger time;

@end

@implementation Countdown

-(instancetype)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

-(void)onEnter {
    
    CCScene *runningScene = [[CCDirector sharedDirector] runningScene];
    [runningScene addChild:_countdownLabel];

    [super onEnter];
    
}

-(id)initWithTime:(NSInteger)time andMessage:(NSString*)message {
    
    if (self = [super init])
    {
        _countdownLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%ld",time] fntFile:@"countdownFont.fnt"];
        _countdownLabel.positionType = CCPositionTypeNormalized;
        _countdownLabel.position = ccp(0.5, 0.5);
        _countdownLabel.alignment = CCTextAlignmentCenter;
        self.time = time;
        [self schedule:@selector(tick:) interval:1.0f];
        
        
    }
    return self;

}

-(void)tick:(CCTime)delta {
    
    [self startCountdown:^(BOOL finished) {
        [_countdownLabel removeFromParent];
        [self.delegate countdownDidEnd];
    }];
    
}

-(void)startCountdown:(countdownEnded) compblock{
    
    if (self.time >0) {
        self.time--;
        [_countdownLabel setString:[NSString stringWithFormat:@"%ld",self.time]];
    } else {
        [self unschedule:@selector(tick:)];
        compblock(YES);

    }
    
}

@end
