//
//  Countdown.h
//  shapes
//
//  Created by Alberto Paladino on 26/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"

@class Countdown;

@protocol CountdownDelegate <NSObject>
-(void)countdownDidEnd;
@end

typedef void(^countdownEnded)(BOOL);

@interface Countdown : CCNode

-(id)initWithTime:(NSInteger)time andMessage:(NSString*)message;

@property (nonatomic, weak) id <CountdownDelegate> delegate;

@end
