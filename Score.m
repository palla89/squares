//
//  Score.m
//  shapes
//
//  Created by Alberto Paladino on 23/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Score.h"

@implementation Score


+(instancetype)sharedInstance
{
    static Score *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Score alloc] init];
    });
    
    return instance;
}

-(void)startUpdatingScore {
    
    [self schedule:@selector(updateScore:) interval:1.0f/30.0f];

}

-(void)stopUpdatingScore {
    
    [self unschedule:@selector(updateScore:)];
}

-(void)resetScore {
    
    GLOBALS.currentScore = 0;
    GLOBALS.scorePointsTaken = 0;
    [self.delegate didUpdateScoreLabelWithScore:GLOBALS.currentScore];
    [self.delegate didUpdateScorePointsLabelWithScorePoints:GLOBALS.currentScore];


}

-(void)updateScore:(CCTime)delta {
    
    GLOBALS.currentScore+=1;
    [self.delegate didUpdateScoreLabelWithScore:GLOBALS.currentScore];
    
}

-(void)addScore:(long long)points {
    
    GLOBALS.currentScore+=points;
    
}

-(void)setScore:(long long)points {
    
    GLOBALS.currentScore=points;
    
}

-(void)setScorePoints:(long)squaresTaken {
    
    GLOBALS.scorePointsTaken = squaresTaken;
    [self.delegate didUpdateScorePointsLabelWithScorePoints:squaresTaken];
    
}

int pointsToUnlockPic = 0;
-(void)addScorePoints {
    
    GLOBALS.scorePointsTaken++;
    pointsToUnlockPic++;
    [self.delegate didUpdateScorePointsLabelWithScorePoints:GLOBALS.scorePointsTaken];
    
    if (pointsToUnlockPic == DEFAULT_DEPIXELLATION_STEP) {
        
        pointsToUnlockPic = 0;
        GLOBALS.currentPercentage += DEFAULT_PERCENTAGE_INCREMENT;
        if ([self.delegatePicture respondsToSelector:@selector(didUpdateUnlockedPhotoPercentual:)]) {
            [self.delegatePicture didUpdateUnlockedPhotoPercentual:GLOBALS.currentPercentage];
        }
    }
    
}

@end