//
//  WinLoseDialog.h
//  shapes
//
//  Created by Alberto Paladino on 27/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "CCNode.h"
#import "MainScene.h"
#import "GameStats.h"

@interface WinLosePauseDialog : CCNode

-(instancetype)initDialogWithGameState:(GameState)gameState andStats:(GameStats*)stats;

@end
