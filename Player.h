//
//  Player.h
//  Shapes
//
//  Created by Alberto Paladino on 10/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Shape.h"

@interface Player : Shape

//properties
@property (nonatomic,getter=isInvulnerable) BOOL invulnerable;


@end
