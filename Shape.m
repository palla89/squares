//
//  Shape.m
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Shape.h"

#define PLAYER_SPEED 100

@interface Shape ()

@property (nonatomic,strong) CCActionSpeed *speedAction;
@property (nonatomic) CGFloat acceleration;

@end

@implementation Shape

-(void)onEnter {
    [super onEnter];
    [self commonInit];
    
}

-(void)commonInit {
    if (self.shapeType != ShapeTypePlayer) {
        if (!self.speed) self.speed = GLOBALS.currentSpeed;
        self.acceleration = GLOBALS.currentAcceleration;
        [self startMovements];
    }
}

-(void)startMovements {
    
    double delay = (double)(arc4random()%40+6)/10;
    //DLog(@"delay %f",delay);
    CCActionDelay *randomDelay = [CCActionDelay actionWithDuration:delay];
    CCActionMoveTo *moveTo;
    CCActionCallBlock *removeShape = [CCActionCallBlock actionWithBlock:^{
        [self removeFromParent];
        [self.delegate didRemoveShapeFromParent:self ofType:self.shapeType];
    }];
    
    CGSize playerSize = self.boundingBox.size;
    CGFloat time = [self calculateMovementDurationFrom:CGPointZero toPosition:ccp(GLOBALS.worldRect.size.width+playerSize.width, 0)];
    switch (self.spawnSide) {
        case SpawnSideDown:
            moveTo = [CCActionMoveTo actionWithDuration:time position:ccp(self.position.x, GLOBALS.worldRect.size.height+playerSize.height)];
            break;
        case SpawnSideUp:
            moveTo = [CCActionMoveTo actionWithDuration:time position:ccp(self.position.x, GLOBALS.worldRect.origin.y-playerSize.height)];
            break;
        case SpawnSideLeft:
            moveTo = [CCActionMoveTo actionWithDuration:time position:ccp(GLOBALS.worldRect.size.width+playerSize.width, self.position.y)];
            break;
        case SpawnSideRight:
            moveTo = [CCActionMoveTo actionWithDuration:time position:ccp(GLOBALS.worldRect.origin.x-playerSize.width,self.position.y)];
            break;
            
        default:
            break;
    }
    //DLog(@"player pos: %f %f",self.position.x,self.position.y);
    CCActionSequence *seq = [CCActionSequence actions:randomDelay,moveTo,removeShape, nil];
    self.speedAction = [CCActionSpeed actionWithAction:seq speed:self.acceleration];

    [self runAction:self.speedAction];
}

-(void)update:(CCTime)delta {
    
    if (self.slowMotionMode) {
        [self.speedAction setSpeed:SLOWMOTION_ACCELERATION];
    } else if (self.fastMotionMode) {
        [self.speedAction setSpeed:FASTMOTION_ACCELERATION];
    }
    else if (!self.fastMotionMode && !self.slowMotionMode) {
        [self.speedAction setSpeed:DEFAULT_ACCELERATION];
    }
}

-(void)setRotationSpeed:(CGFloat)rotationSpeed {
    
    _rotationSpeed = rotationSpeed;
    if (rotationSpeed==0) return;
    [self stopActionByTag:kActionTagRotateShape];
    [self rotateWithSpeed:rotationSpeed];
    
}

-(void)rotateWithSpeed:(CGFloat)rotationSpeed {
    
    CCActionRotateBy *rotate = [CCActionRotateBy actionWithDuration:rotationSpeed
                                                              angle:60];
    CCActionRepeatForever *rotateForever =
    [CCActionRepeatForever actionWithAction:rotate];
    rotateForever.tag = kActionTagRotateShape;
    [self runAction:rotateForever];

}

-(CGFloat)calculateMovementDurationFrom:(CGPoint)startPos toPosition:(CGPoint)endPos {
    float distance = ccpDistance(startPos, endPos);
    float duration = distance/((arc4random()%7+self.speed)*10);
    //DLog(@"%f",duration);
    return duration;
    
}

@end
