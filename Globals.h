//
//  Globals.h
//  Shapes
//
//  Created by Alberto Paladino on 10/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Shape.h"

//utils
#define GLOBALS [Globals sharedInstance]
#define WINSIZE [CCDirector sharedDirector].viewSize

//general vars
#define GAMEFIELD_SIZE CGSizeMake(WINSIZE.width*GAMEFIELD_SCALE_COEFFICIENT, WINSIZE.height*GAMEFIELD_SCALE_COEFFICIENT)
#define GAMEFIELD_SCALE_COEFFICIENT 4
#define OFFSCREEN_OFFSET 50
#define BONUS_NUMBER 4
#define MALUS_NUMBER 4
#define LIVES_NUMBER 1
#define BONUS_MALUS_SPAWN_DELAY 10
#define SCALE_LIMIT_LOWER 0.5
#define SCALE_LIMIT_UPPER 2.5
#define LEVEL_UP_TARGET 5

//default/startup params
#define DEFAULT_SHAPE_SPEED 10
#define DEFAULT_BONUS_MALUS_SPEED 12
#define DEFAULT_ENEMIES_NUMBER 50
#define DEFAULT_SCOREPOINTS_NUMBER 30
#define DEFAULT_ACCELERATION 1
#define DEFAULT_SCALE_VALUE 1
#define DEFAULT_PIXELLATE_BLOCK 180
#define DEFAULT_DEPIXELLATION_STEP 1

//bonus
#define SLOWMOTION_ACCELERATION 0.3
#define BONUS_RESCALE 0.7
#define DEFAULT_BONUS_DURATION 5

//malus
#define FASTMOTION_ACCELERATION 2
#define MALUS_RESCALE 1.3
#define DEFAULT_MALUS_DURATION 5

//incremental values
#define SHAPE_SPEED_INCREMENT 2
#define ENEMIES_INCREMENT 1
#define SCOREPOINTS_INCREMENT 1
#define ZOOM_SCALE_FACTOR 1.02
#define PIXELLATE_STEP 5
#define DEFAULT_SCORE_INCREMENT 1
#define DEFAULT_PERCENTAGE_INCREMENT 2

//score values
#define SCOREPOINT_POINTS 3000
#define BONUS_POINTS 10000
#define MALUS_POINTS -10000

//nodes names
#define kNamePlayer @"kNamePlayer"

//action tags
#define kActionTagRotateShape 1000

//particles
#define PARTICLE_BACKGROUND_DEFAULT_SPEED 1.0
#define PARTICLE_BACKGROUND_DEFAULT_RADIALACC 30.0

//collision groups
static const NSString *kCollisionGroupPlayer = @"playerGroup";
static const NSString *kCollisionGroupEnemy = @"enemyGroup";
static const NSString *kCollisionGroupScorePoint = @"scorePointGroup";
static const NSString *kCollisionGroupBonus =@"bonusGroup";
//collision types
static NSString *kCollisionTypePlayer = @"playerCollision";
static NSString *kCollisionTypeEnemy = @"enemyCollision";
static NSString *kCollisionTypeScorePoint = @"scorePointCollision";
static NSString *kCollisionTypeBonus = @"bonusCollision";
static NSString *kCollisionTypeMalus = @"malusCollision";

@interface Globals : NSObject

@property (nonatomic,readwrite) NSInteger globalShapeSpeed;
@property (nonatomic,readwrite) NSInteger minEnemies;
@property (nonatomic,readwrite) NSInteger minScorePoints;
@property (nonatomic,readwrite) CGFloat enemyScale;
@property (nonatomic,readwrite) CGFloat scorePointScale;
@property (nonatomic,readwrite) NSInteger scorePointsTaken;
@property (nonatomic,readwrite) CGFloat currentSpeed;
@property (nonatomic,readwrite) CGFloat currentBonusMalusSpeed;
@property (nonatomic,readwrite) CGFloat currentAcceleration;
@property (nonatomic,readwrite) long long currentScore;
@property (nonatomic,readwrite) long long currentScoreIncrement;
@property (nonatomic,readwrite) NSInteger currentLevel;
@property (nonatomic,readwrite) NSInteger currentLives;
@property (nonatomic,readwrite) NSInteger currentPercentage;

@property (nonatomic) CGRect worldRect;

+(instancetype)sharedInstance;

-(CGPoint)spawnNode:(Shape*)shape atSpawnSide:(SpawnSide)side inScene:(BOOL)inOrOut;
- (CCSprite*)blankSpriteWithSize:(CGSize)size;
#pragma mark - Utils

-(BOOL)isIphone;

@end