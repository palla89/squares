
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// GBDeviceInfo
#define COCOAPODS_POD_AVAILABLE_GBDeviceInfo
#define COCOAPODS_VERSION_MAJOR_GBDeviceInfo 3
#define COCOAPODS_VERSION_MINOR_GBDeviceInfo 1
#define COCOAPODS_VERSION_PATCH_GBDeviceInfo 0

// HexColors
#define COCOAPODS_POD_AVAILABLE_HexColors
#define COCOAPODS_VERSION_MAJOR_HexColors 2
#define COCOAPODS_VERSION_MINOR_HexColors 3
#define COCOAPODS_VERSION_PATCH_HexColors 0

