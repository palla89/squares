//
//  Enemy.m
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "ScorePoint.h"

@implementation ScorePoint

-(void)onEnter {
    
    [super onEnter];
    
    
}

-(id)init {
    
    if ((self = [super init])) {

        self.spawnSide = arc4random()%4;
        self.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"square.png"];
        self.shapeType = ShapeTypeScorePoint;
        self.color = [CCColor greenColor];
        self.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){self.position, self.contentSize} cornerRadius:0];
        self.position = [GLOBALS spawnNode:self atSpawnSide:self.spawnSide inScene:NO];
        self.physicsBody.collisionGroup = kCollisionGroupScorePoint;
        self.physicsBody.collisionType = kCollisionTypeScorePoint;
        self.speed = GLOBALS.currentSpeed;
        self.scale = GLOBALS.scorePointScale;
        if (!self.rotationSpeed) {
            CGFloat randomSpeed =arc4random()%5+1;
         self.rotationSpeed = 2/randomSpeed+0.1;
        }
   
    }
    return self;
}


@end
