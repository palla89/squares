//
//  WinLoseDialog.m
//  shapes
//
//  Created by Alberto Paladino on 27/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "WinLosePauseDialog.h"
#import "MenuScene.h"
#import "CCNode+Scaling.h"

#define kFontName @"Phosphate-Inline"
#define kStatsFontSize 35

#define kKeyX           0.2f

#define kValueX         0.8f

#define kLine1Y                 0.7f
#define kMarginBetweenLines     0.08f

@implementation WinLosePauseDialog {
    
    CCLayoutBox *_menu;
    MainScene *_mainScene;
}

-(instancetype)initDialogWithGameState:(GameState)gameState andStats:(GameStats*)stats {
    
    if (self = [super init])
    {

        [self setupModalDialog];
        [self createDialogLayoutWithGameState:gameState andStats:stats];
    }
    
    return self;
}

-(void)setupModalDialog
{
    self.contentSizeType = CCSizeTypeNormalized;
    self.contentSize = CGSizeMake(1, 1);
    self.userInteractionEnabled = YES;
}

-(void)createDialogLayoutWithGameState:(GameState)gameState andStats:(GameStats*)stats
{
    
    CCSprite *bg =
    [CCSprite spriteWithImageNamed:
     @"menu-dialog.png"];
    
    bg.positionType = CCPositionTypeNormalized;
    bg.position = ccp(0.5f, 0.5f);
    [bg scaleToSize:CGSizeMake(WINSIZE.width*0.8, WINSIZE.height*0.8) fitType:CCScaleFitAspectFit];
    
    [self addChild:bg];
    
    switch (gameState) {
        case GameStateLost: {
            _mainScene = [[MainScene alloc] init];
            [_mainScene resetGame];

            CCLabelTTF *gameOverLabel = [CCLabelTTF labelWithString:@"Game Over" fontName:kFontName fontSize:70];
            gameOverLabel.positionType = CCPositionTypeNormalized;
            gameOverLabel.position = ccp(0.5,0.85);
            [bg addChild:gameOverLabel];
        }
            break;
            
        case GameStatePaused: {
            CCLabelTTF *pauseLabel = [CCLabelTTF labelWithString:@"Paused" fontName:kFontName fontSize:70];
            pauseLabel.positionType = CCPositionTypeNormalized;
            pauseLabel.position = ccp(0.5,0.85);
            [bg addChild:pauseLabel];
            
        }
        default:
            break;
    }
    
    NSDictionary *gamestats =
    @{ @"Squares Taken" :
           [NSString stringWithFormat:@"%ld", stats.squaresTaken],
       @"Progress completion" :
           [NSString stringWithFormat:@"%ld%%", stats.percentUnlocked],
       };
    
    float margin = 0;
    CCColor *fontColor = [CCColor blackColor];
    
    for (NSString *key in gamestats.allKeys)
    {
        CCLabelTTF *lblKey =
        [CCLabelTTF labelWithString:key
                           fontName:kFontName
                           fontSize:kStatsFontSize];
        lblKey.color = fontColor;
        lblKey.anchorPoint = ccp(0.0f, 0.5f);
        lblKey.positionType = CCPositionTypeNormalized;
        lblKey.position = ccp(kKeyX, kLine1Y - margin);
        [bg addChild:lblKey];
        
        CCLabelTTF *lblValue =
        [CCLabelTTF labelWithString:[gamestats objectForKey:key]
                           fontName:kFontName
                           fontSize:kStatsFontSize];
        lblValue.color = fontColor;
        lblValue.anchorPoint = ccp(1.0f, 0.5f);
        lblValue.positionType = CCPositionTypeNormalized;
        lblValue.position = ccp(kValueX, kLine1Y - margin);
        [bg addChild:lblValue];
        
        margin += kMarginBetweenLines;
    }
    
    CCSpriteFrame *restartNormalImage =
    [CCSpriteFrame frameWithImageNamed:@"rectangle-up.png"];
    CCSpriteFrame *restartHighLightedImage =
    [CCSpriteFrame
     frameWithImageNamed:@"rectangle-down.png"];
    CCButton *btnRestart =
    [CCButton buttonWithTitle:@"Restart"
                  spriteFrame:restartNormalImage
       highlightedSpriteFrame:restartHighLightedImage
          disabledSpriteFrame:nil];
    btnRestart.label.fontSize = 50;
    btnRestart.positionType = CCPositionTypeNormalized;
    btnRestart.position = ccp(0.25f, 0.2f);
    [btnRestart setTarget:self
                 selector:@selector(btnRestartTapped:)];
    [bg addChild:btnRestart];
    
    CCSpriteFrame *exitNormalImage =
    [CCSpriteFrame frameWithImageNamed:@"rectangle-up.png"];
    CCSpriteFrame *exitHighLightedImage =
    [CCSpriteFrame
     frameWithImageNamed:@"rectangle-down.png"];
    CCButton *btnExit =
    [CCButton buttonWithTitle:@"Exit"
                  spriteFrame:exitNormalImage
       highlightedSpriteFrame:exitHighLightedImage
          disabledSpriteFrame:nil];
    btnExit.positionType = CCPositionTypeNormalized;
    btnExit.position = ccp(0.75f, 0.2f);
    btnExit.label.fontSize = 50;

    [btnExit setTarget:self
              selector:@selector(btnExitTapped:)];
    [bg addChild:btnExit];
}

-(void)btnRestartTapped:(id)sender
{
    [[CCDirector sharedDirector]
     replaceScene:_mainScene];
}

-(void)btnExitTapped:(id)sender
{
    MenuScene *menuScene = [[MenuScene alloc] init];
    CCTransition *transition =
    [CCTransition transitionCrossFadeWithDuration:1.0f];
    
    [[CCDirector sharedDirector]
     replaceScene:menuScene withTransition:transition];
}

-(void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    //do nothing, swallow touch
}



@end
