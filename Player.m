//
//  Player.m
//  Shapes
//
//  Created by Alberto Paladino on 10/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Player.h"

@interface Player ()

@property (nonatomic,getter=isActionRunning) BOOL actionRunning;
@property (nonatomic,strong) CCParticleSystem *aura;

@end

@implementation Player

-(void)onEnter {
    
    [super onEnter];
    
}

-(id)init {
    
    if ((self = [super init])) {
        self.name = kNamePlayer;
        self.userInteractionEnabled = YES;
        self.actionRunning = NO;
        self.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"player.png"];
        self.color = [CCColor yellowColor];
        self.rotationSpeed = 0.6;
        self.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(self.position.x+self.contentSize.width/4, self.position.y+self.contentSize.height/4), CGSizeMake(self.contentSize.width/2, self.contentSize.height/2)} cornerRadius:0];
        self.physicsBody.collisionGroup = kCollisionGroupPlayer;
        self.physicsBody.collisionType = kCollisionTypePlayer;
        self.shapeType = ShapeTypePlayer;

        self.position =ccp((WINSIZE.width-self.scene.boundingBox.size.width)/2.0f, (WINSIZE.height-self.scene.boundingBox.size.height)/2.0f);

        
        //self.invulnerable=YES;
    }
    return self;
}

#pragma mark - Player settings

-(void)setInvulnerable:(BOOL)invulnerable {
    
    _invulnerable = invulnerable;
    
    if (invulnerable) {

        if (!self.aura) {
            self.aura = [CCParticleSystem particleWithFile:@"invulnerability.plist"];
            self.aura.particlePositionType = CCParticleSystemPositionTypeRelative;
            self.aura.positionType = CCPositionTypeNormalized;
            self.aura.position = ccp(0.5, 0.5);
            [self addChild:self.aura];
        }
        self.physicsBody.collisionGroup = kCollisionGroupEnemy;
        self.color = [CCColor whiteColor];
    } else {
        if (self.aura) {
            [self.aura removeFromParent];
            self.aura = nil;
        }
        self.physicsBody.collisionGroup = kCollisionGroupPlayer;
        self.color = [CCColor yellowColor];
    }
}

#pragma mark - Touch methods

-(void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    
    if (!self.isActionRunning) {
        self.actionRunning = YES;
        CCActionScaleBy *zoom = [CCActionScaleBy actionWithDuration:0.1 scale:1.1];
        CCActionScaleBy *normalize = [CCActionScaleBy actionWithDuration:0.1 scale:0.9];
        CCActionCallFunc *endOfAction = [CCActionCallBlock actionWithBlock:^{
            self.actionRunning = NO;
        }];
        CCActionSequence *seq = [CCActionSequence actions:zoom,normalize,endOfAction, nil];

        [self runAction:seq];
    }
    
}

-(void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event {
    
    
//    CCLOG(@"****finger moving at locinNode parent : (%f, %f)",
//          [touch locationInNode:self.parent].x,
//          [touch locationInNode:self.parent].y);
    
    CGPoint touchLocation = [touch locationInNode:self.parent];
    self.position = ccp(touchLocation.x,touchLocation.y);
    
}

@end