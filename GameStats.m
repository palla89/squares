//
//  GameStats.m
//  shapes
//
//  Created by Alberto Paladino on 06/09/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "GameStats.h"

@implementation GameStats

-(instancetype)init {
    
    if (self = [super init]) {
        
        self.squaresTaken = GLOBALS.scorePointsTaken;
        self.percentUnlocked = 20;
        
    }
    
    return self;
}

@end
