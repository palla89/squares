//
//  HudLayer.m
//  shapes
//
//  Created by Alberto Paladino on 22/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "HudLayer.h"
#import <HexColors.h>

#define kFontName @"Phosphate-Inline"
#define kFontSize 14


@interface HudLayer ()

@property (nonatomic, strong) Score *score;

@end

@implementation HudLayer {
    
    CCLabelTTF *_scoreText;
    CCLabelBMFont *_scoreLabel;
    CCLabelTTF *_livesLabel;
    CCButton *_pauseButton;
    CCLabelTTF *_pauseLabel;
    CCLabelTTF *_squaresNumberLabel;
    CCProgressNode *_progressBarUpper;
    CCProgressNode *_progressBarLower;
    float labelsY;
    float labelsPaddingX;
}

-(instancetype)init
{
    if (self = [super init])
    {
        self.score = [Score sharedInstance];
        _scoreText = [CCLabelTTF
                  labelWithString:@"Score:"
                  fontName:kFontName
                  fontSize:kFontSize];
        _scoreLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%llu",GLOBALS.currentScore] fntFile:@"scoreFont.fnt"];
        _livesLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Lives: %ld",GLOBALS.currentLives]
                  fontName:kFontName
                  fontSize:kFontSize];
        _squaresNumberLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Squares taken: %ld",GLOBALS.scorePointsTaken]
                                         fontName:kFontName
                                         fontSize:kFontSize];
        
        _scoreText.color =
        [CCColor colorWithUIColor:[UIColor colorWithHexString:@"ECF0F1"]];
        _livesLabel.color =
        [CCColor colorWithUIColor:[UIColor colorWithHexString:@"F22613"]];
        _squaresNumberLabel.color =
        [CCColor colorWithUIColor:[UIColor colorWithHexString:@"F22613"]];

        labelsY = WINSIZE.height * 0.95f;
        labelsPaddingX = WINSIZE.width * 0.01f;
        
        CCSpriteFrame *pauseNormalImage =
        [CCSpriteFrame frameWithImageNamed:@"pause-up.png"];
        
        CCSpriteFrame *pauseHighlightedImage =
        [CCSpriteFrame
         frameWithImageNamed:@"pause-down.png"];
        _pauseButton = [CCButton buttonWithTitle:nil spriteFrame:pauseNormalImage highlightedSpriteFrame:pauseHighlightedImage disabledSpriteFrame:nil];
        _pauseButton.anchorPoint = ccp(0, 0.5f);
        _pauseButton.position = ccp(WINSIZE.width - _pauseButton.boundingBox.size.width, labelsY);
        [_pauseButton setTarget:self selector:@selector(pauseGame:)];
        
        _scoreText.anchorPoint = ccp(0, 0.5f);
        _scoreText.position = ccp(labelsPaddingX, labelsY);
        
        _scoreLabel.anchorPoint = ccp(0,0.5f);
        _scoreLabel.position = ccp(_scoreText.position.x+_scoreText.boundingBox.size.width,labelsY);
        
        _livesLabel.anchorPoint = ccp(0.9, 0.5f);
        _livesLabel.position = ccp(WINSIZE.width - _pauseButton.boundingBox.size.width*2, labelsY);
        
        _squaresNumberLabel.anchorPoint = ccp(0.5f,0.5f);
        _squaresNumberLabel.position = ccp(WINSIZE.width/2,labelsY);
        
        [self addChild:_scoreText];
        [self addChild:_scoreLabel];
        [self addChild:_livesLabel];
        [self addChild:_squaresNumberLabel];
        [self addChild:_pauseButton];
        
    }
    
    return self;
}

-(void)pauseGame:(id)sender {
    
    if (![[CCDirector sharedDirector] isPaused]) {
        
        [[CCDirector sharedDirector] pause];
        _pauseLabel = [CCLabelTTF labelWithString:@"GAME PAUSED" fontName:kFontName fontSize:50];

        _pauseLabel.anchorPoint = ccp(0.5f, 0.5f);

        _pauseLabel.position = ccp(WINSIZE.width/2,WINSIZE.height/2);
        [self addChild:_pauseLabel];
        
    } else {
        [self removeChild:_pauseLabel];
        [[CCDirector sharedDirector] resume];
    }
}

-(void)setScore:(Score *)score {
    
    _score = score;
    _score.delegate = self;
}

-(void)setLives:(Lives *)lives {
    
    _lives = lives;
    _lives.delegate = self;
}

-(void)didUpdateScoreLabelWithScore:(long long)score {
    
    [_scoreLabel setString:[NSString stringWithFormat:@"%lli",score]];
}

-(void)didUpdateLivesLabelWithLives:(NSInteger)livesNumber {
    
    [_livesLabel setString:[NSString stringWithFormat:@"Lives: %ld",livesNumber]];
}

-(void)didUpdateScorePointsLabelWithScorePoints:(long)squaresTaken {
    
    [_squaresNumberLabel setString:[NSString stringWithFormat:@"Squares taken: %ld",squaresTaken]];
    
}

-(void)showProgressBarOfType:(ProgressBarPosition)position Color:(CCColor*)color ticking:(BOOL)ticking timerToEnd:(CCTime)time {
    
    CCSprite *sprite = [GLOBALS blankSpriteWithSize:CGSizeMake(150, 5)];
    sprite.color = color;
    
    if (position == ProgressBarPositionUpper) {
        _progressBarUpper = [CCProgressNode progressWithSprite:sprite];
        _progressBarUpper.type = CCProgressNodeTypeBar;
        _progressBarUpper.midpoint = ccp(0.0f, 0.0f);
        _progressBarUpper.barChangeRate = ccp(1.0f, 0.0f);
        _progressBarUpper.percentage = 100.0f;
        _progressBarUpper.anchorPoint = ccp(0.5f,0.5f);
        
        _progressBarUpper.position = ccp(WINSIZE.width/2,25);
        [self addChild:_progressBarUpper];
        
        if (ticking) {
            
            [self schedule:@selector(updateProgressBarUpper:) interval:1.0f/60.0f];
        }
    } else {
        
        _progressBarLower = [CCProgressNode progressWithSprite:sprite];
        _progressBarLower.type = CCProgressNodeTypeBar;
        _progressBarLower.midpoint = ccp(0.0f, 0.0f);
        _progressBarLower.barChangeRate = ccp(1.0f, 0.0f);
        _progressBarLower.percentage = 100.0f;
        _progressBarLower.anchorPoint = ccp(0.5f,0.5f);
        
        _progressBarLower.position = ccp(WINSIZE.width/2,10);
        [self addChild:_progressBarLower];
        
        if (ticking) {
            
            [self schedule:@selector(updateProgressBarLower:) interval:1.0f/60.0f ];
        }
        
    }
    
}

-(void)stopAndRemoveProgressBar:(ProgressBarPosition)position {
    
    if (position == ProgressBarPositionUpper) {
       // [self unschedule:@selector(updateProgressBarUpper:)];
        [_progressBarUpper removeFromParentAndCleanup:YES];
    } else {
       // [self unschedule:@selector(updateProgressBarLower:)];
        [_progressBarLower removeFromParentAndCleanup:YES];
    }
    
}
//time / (1/60) -- TODO: Rendere dinamico, fare il calcolo aggiungendo qualcosina in piu
-(void)updateProgressBarUpper:(CCTime)delta {
    _progressBarUpper.percentage-=0.35;
    
    if (_progressBarUpper.percentage<=0) {
        [self unschedule:@selector(updateProgressBarUpper:)];
        [_progressBarUpper removeFromParentAndCleanup:YES];
    }
}

-(void)updateProgressBarLower:(CCTime)delta {
    _progressBarLower.percentage-=0.35;
    
    if (_progressBarLower.percentage<=0) {
        [self unschedule:@selector(updateProgressBarLower:)];
        [_progressBarLower removeFromParentAndCleanup:YES];
    }
}

@end
