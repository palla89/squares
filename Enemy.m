//
//  Enemy.m
//  Shapes
//
//  Created by Alberto Paladino on 09/07/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "Enemy.h"

@implementation Enemy

-(void)onEnter {
    
    [super onEnter];
    
}

-(id)init {
    
    if ((self = [super init])) {
        
        self.spawnSide = arc4random()%4;
        self.shapeType = ShapeTypeEnemy;
        self.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"square.png"];
        self.color = [CCColor redColor];
        self.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){self.position, self.contentSize} cornerRadius:0];
        self.position = [GLOBALS spawnNode:self atSpawnSide:self.spawnSide inScene:NO];
        self.physicsBody.collisionGroup = kCollisionGroupEnemy;
        self.physicsBody.collisionType = kCollisionTypeEnemy;
        self.speed = GLOBALS.currentSpeed;
        self.scale = GLOBALS.enemyScale;
        if (!self.rotationSpeed) {
            CGFloat randomSpeed =arc4random()%5+1;
            self.rotationSpeed = 2/randomSpeed+0.1;
        }
        
    }
    return self;
}


@end
