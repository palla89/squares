//
//  MenuScene.m
//  shapes
//
//  Created by Alberto Paladino on 27/08/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#import "MenuScene.h"
#import "MainScene.h"

#define kFontName @"Phosphate-Inline"
#define kFontSize 20

@implementation MenuScene {

    CCLayoutBox *_menu;
}

-(instancetype)init
{
    if (self = [super init])
    {
        [self addBackground];
        [self addMenuButtons];
    }
    
    return self;
}

-(void)addBackground
{
    CCSprite *bg = [CCSprite
                    spriteWithImageNamed:@"bg-menu.jpg"];
    bg.positionType = CCPositionTypeMake(CCPositionUnitNormalized,
                                          CCPositionUnitNormalized,
                                          CCPositionReferenceCornerBottomLeft);
    bg.position = ccp(0.5f, 0.5f);
    [self addChild:bg];
}

-(void)addMenuButtons
{
    
    CCSpriteFrame *startNormalImage =
    [CCSpriteFrame frameWithImageNamed:@"btn_start.png"];
    
    
    CCSpriteFrame *startHighlightedImage =
    [CCSpriteFrame
     frameWithImageNamed:@"btn_start_pressed.png"];
    
    CCButton *btnStart = [CCButton buttonWithTitle:@"Start game"
                                       spriteFrame:startNormalImage
                            highlightedSpriteFrame:startHighlightedImage
                               disabledSpriteFrame:nil];
    btnStart.label.fontName = kFontName;
    btnStart.label.fontSize = kFontSize;

    [btnStart setTarget:self
               selector:@selector(btnStartTapped:)];
    
    CCSpriteFrame *aboutNormalImage =
    [CCSpriteFrame frameWithImageNamed:@"btn_about.png"];
    CCSpriteFrame *aboutHighlightedImage =
    [CCSpriteFrame
     frameWithImageNamed:@"btn_about_pressed.png"];
    CCButton *btnAbout =
    [CCButton buttonWithTitle:@"Settings"
                  spriteFrame:aboutNormalImage
       highlightedSpriteFrame:aboutHighlightedImage
          disabledSpriteFrame:nil];
    btnAbout.label.fontName = kFontName;
    btnAbout.label.fontSize = kFontSize;
    [btnAbout setTarget:self
               selector:@selector(btnAboutTapped:)];
    
    CCSpriteFrame *highscoresNormalImage =
    [CCSpriteFrame
     frameWithImageNamed:@"btn_high“scores.png"];
    CCSpriteFrame *highscoresHighlightedImage =
    [CCSpriteFrame
     frameWithImageNamed:@"btn_highscores_pressed.png"];
    CCButton *btnHighscores =
    [CCButton buttonWithTitle:@"HighScores"
                  spriteFrame:highscoresNormalImage
       highlightedSpriteFrame:highscoresHighlightedImage
          disabledSpriteFrame:nil];
    btnHighscores.label.fontName = kFontName;
    btnHighscores.label.fontSize = kFontSize;

    [btnHighscores setTarget:self
                    selector:@selector(btnHighscoresTapped:)];
    
    _menu = [[CCLayoutBox alloc] init];
    _menu.direction = CCLayoutBoxDirectionVertical;
    _menu.spacing = 40.0f;
    
    [_menu addChild:btnHighscores];
    [_menu addChild:btnAbout];
    [_menu addChild:btnStart];
    
    [_menu layout];
    
    _menu.anchorPoint = ccp(0.5f, 0.5f);
    _menu.positionType =
    CCPositionTypeMake(CCPositionUnitNormalized,
                       CCPositionUnitNormalized,
                       CCPositionReferenceCornerBottomLeft);
    _menu.position = ccp(0.5f, 0.5f);
    
    [self addChild:_menu];
}

-(void)btnStartTapped:(id)sender
{
    
    [[CCDirector sharedDirector]
     replaceScene:[[MainScene alloc] init]];
}

-(void)btnAboutTapped:(id)sender
{
    CCLOG(@"About Tapped");
}

-(void)btnHighscoresTapped:(id)sender
{
    CCLOG(@"Highscores Tapped");
}




@end
